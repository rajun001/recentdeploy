from django.db import models
from federation.models import Federation
# Create your models here.


class Society(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    full_name = models.CharField(max_length=50)
    mobile = models.BigIntegerField()
    password = models.TextField(max_length=10)
    role = models.CharField(max_length=20, null=True, blank=True, default="SOCIETY")
    federation = models.ForeignKey(Federation,on_delete=models.PROTECT)