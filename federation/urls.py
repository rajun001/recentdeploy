
from django.conf.urls import url, include
from django.contrib import admin
from .views import *




urlpatterns = [
    url(r'^contractor_profile',contractor_profile),
    url(r'^addsociety',addsociety),
    url(r'^uploadxsl', uploadxsl),
    url(r'^registrationdata', storeregidata),

]
