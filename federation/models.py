from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db import models



class Federation(models.Model):
    user = models.OneToOneField(User,null=True, on_delete=models.CASCADE)
    id = models.CharField(max_length=50,primary_key=True)
    federation_name = models.CharField(null=True,max_length=50)
    mobile = models.BigIntegerField()
    emailid = models.EmailField(null=True)
    password = models.TextField(max_length=20)
    account_number = models.BigIntegerField(null=True)
    account_name = models.CharField(max_length=50)
    account_type = models.CharField(null=True,max_length=50)
    bank_name = models.CharField(null=True,max_length=50)
    branch_name = models.CharField(null=True,max_length=50)
    date = models.DateField(null=True)
    ifsc_code =models.TextField(null=True,max_length=20)
    addhar_number = models.BigIntegerField(null=True)
    pan_number = models.TextField(null=True,max_length=20)
    houseno = models.CharField(max_length=10,null=True)
    street = models.CharField(max_length=100,null=True)
    landmark = models.CharField(max_length=50, null=True)
    city =models.CharField(null=True,max_length=50)
    state = models.CharField(null=True,max_length=50)
    pin_code = models.BigIntegerField(null=True)
    role = models.CharField(max_length=20, null=True, blank=True, default="FEDERATION")



class Shcildata(models.Model):
    account_id = models.CharField(max_length=50,null=True)
    branch_name = models.CharField(max_length=50,null=True)
    member_bank = models.CharField(max_length=200,null=True)
    total_cert = models.IntegerField(null=True)
    total_amt = models.BigIntegerField(null=True)


class Bankdata(models.Model):
    cust_id = models.BigIntegerField(null=True)
    parent_account_number = models.BigIntegerField(null=True)
    acct_name = models.CharField(max_length=200,null=True)
    virtual_acct_number=models.TextField(max_length=200,null=True)
    remitter_name= models.CharField(max_length=200,null=True)
    proposed_account_title = models.CharField(max_length=50,null=True)
    utr = models.CharField(max_length=50,null=True)
    tran_amt = models.IntegerField(null=True)
    paysys_id = models.CharField(max_length=50,null=True)
    sender_to_receiver_iformation = models.CharField(max_length=50,null=True)
    tran_date = models.DateField(null=True)
    tran_time = models.TimeField(null = True)



class Addsociety(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    society_name = models.CharField(null=True, max_length=50)
    mobile = models.BigIntegerField()
    emailid = models.EmailField(null=True)
    password = models.TextField(max_length=10)
    registration_date = models.DateTimeField(null=True)
    aadhaar_number = models.BigIntegerField(null=True)
    pan_number = models.TextField(null=True, max_length=20)
    account_number = models.BigIntegerField(null=True)
    account_name = models.CharField(max_length=50)
    account_type = models.CharField(null=True, max_length=50)
    bank_name = models.CharField(null=True, max_length=50)
    branch_name = models.CharField(null=True, max_length=50)
    ifsc_code = models.TextField(null=True, max_length=20)
    house_number = models.CharField(null=True, max_length=10)
    street_name = models.CharField(null=True, max_length=50)
    land_mark = models.CharField(null=True, max_length=50)
    city = models.CharField(null=True, max_length=50)
    state = models.CharField(null=True, max_length=50)
    pincode = models.BigIntegerField(null=True)
    federation_id = models.IntegerField(null=True)
    role = models.CharField(max_length=20, null=True, blank=True, default="SOCIETY")