from datetime import datetime

from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from django.shortcuts import render
from rest_framework.decorators import api_view
from administration.functions import authentication,encrypt_password
from django.contrib.auth.models import User
from .models import Federation,Bankdata
from rest_framework.response import Response
from .models import Addsociety



# Create your views here.



@api_view(['POST'])
def contractor_profile(request):
    id = authentication(request.META['HTTP_AUTHORIZATION'])
    mobile = request.data['mobile']
    full_name = request.data['full_name']
    password = request.data['password']
    emailid = request.data['emailid']
    encrypt = encrypt_password(password)

    user = User()
    user.username = mobile
    user.password = encrypt
    user.first_name = full_name
    user.save()
    userid = User.objects.get(id=user.id)
    ferderation = Federation()
    ferderation.ferderation_name = full_name
    ferderation.mobile = mobile
    ferderation.password = encrypt
    ferderation.emailid = emailid
    # cont.parking_lots = parkingLots
    ferderation.user = userid
    ferderation.save()
    return Response("Successfully saved contractor")



@api_view(['POST'])
def storeregidata(request):
    print ("hiiiiiiiiiiiiii",request.data)
    federation_name = request.data['firstName']
    mobile = request.data['mobilenumber']
    emailid = request.data['email']
    password = request.data['password']
    try:
        user = User.objects.get(username=mobile,is_active=True)
        print("user11111",user)
        if user:
            return Response("User Already Exists"   )
    except User.DoesNotExist:
        print("inside except")
        try:
            with transaction.atomic():
                # user = User.objects.get(username=mobile)
                # print("user",user)
                userdata = User()
                userdata.last_name = ""
                userdata.username = mobile
                encrypt= encrypt_password(password)
                userdata.password = encrypt
                userdata.first_name = federation_name
                userdata.email = emailid
                userdata.is_active = True
                # userdata.id = get_federation_id()
                userdata.save()
                # userid = User.objects.get(id = userdata.id).value('id')
                print("userdataasddad",userdata.id  , type(userdata.id))
                # userid = User.objects.get(username=mobile)
                # print("sachi",userid.id)
                resgidata = Federation()
                resgidata.id = get_federation_id()
                resgidata.federation_name = federation_name
                resgidata.mobile = mobile
                resgidata.emailid = emailid
                resgidata.password = encrypt
                resgidata. account_number = request.data['accountnumber']
                resgidata.account_name = request.data['accountName']
                resgidata. account_type = request.data['accountType']
                resgidata.user_id = int(userdata.id)
                resgidata.bank_name = request.data['bankName']
                resgidata. branch_name = request.data['branchName']
                resgidata.date = request.data['datepickerModel'][0:10]
                resgidata. ifsc_code = request.data['ifscCode']
                resgidata.addhar_number = request.data['aharNumbe']
                resgidata.pan_number = request.data['panNumber']
                resgidata.houseno = request.data['houseNumber']
                resgidata.street = request.data['street']
                resgidata.landmark = request.data['landMark']
                resgidata.city = request.data['city']
                resgidata.state = request.data['state']
                resgidata. pin_code = request.data['pinCode']
                resgidata.save()
                return Response("success",status=status.HTTP_200_OK)

        except Exception as e:
            print(e)
            return Response("Registration Failed")



@api_view(['POST'])
def uploadxsl(request):
    print("aaaaaaaaaaaa",request.data)
    a = request.data

    for i in range(0,len(a)-1):
        bankdata = Bankdata()
        print(a[i]['CUST_ID'])
        bankdata.cust_id = int(a[i]['CUST_ID'])
        bankdata.parent_account_number = a[i]['PARENT_ACCOUNT_NUMBER']
        bankdata.acct_name =a[i]['ACCT_NAME']
        bankdata.virtual_acct_number = a[i]['VIRTUAL_ACCT_NUMBER']
        bankdata.remitter_name = a[i]['REMITTER_NAME']
        bankdata.proposed_account_title = a[i]['PROPOSED_ACCOUNT_TITLE']
        bankdata.utr = a[i]['UTR']
        bankdata.tran_amt = a[i]['TRAN_AMT']
        bankdata.paysys_id =a[i]['PAYSYS_ID']
        bankdata.sender_to_receiver_iformation =a[i]['SENDER_TO_RECEIVER_INFORMATION']
        s =a[i]['TRAN_DATE_AND_TIME'].split("   ")
        bankdata.tran_date = datetime.strptime(s[0],'%d.%m.%Y').date()
        bankdata.tran_time = datetime.strptime(s[1],' %I:%M:%S %p').time()
        bankdata.save()
    return Response("Bank data saved Successfully")


@api_view(['POST'])
def addsociety(request):
    print ("society details from frontenddddddd", request.data)
    society_name = request.data['firstName']
    mobile = request.data['mobilenumber']
    emailid = request.data['email']
    password = request.data['password']
    registration_date = request.data['Registration']
    aadhaar_number = request.data['aharNumbe']
    pan_number = request.data['panNumber']
    account_number = request.data['accountnumber']
    account_name = request.data['accountName']
    account_type = request.data['accountType']
    bank_name = request.data['bankName']
    branch_name = request.data['branchName']
    ifsc_code = request.data['ifscCode']
    house_number = request.data['houseNumber']
    street_name = request.data['street']
    land_mark = request.data['landMark']
    city = request.data['city']
    state = request.data['state']
    pincode = request.data['pinCode']
    encrypt = encrypt_password(password)
    try:
        user = User.objects.get(username=mobile, is_active=True)
        print("user11111", user)
        if user:
            return Response("User Already Exists")
    except User.DoesNotExist:
        print("inside except")
        try:
    
            with transaction.atomic():

                user = User()
                user.username = mobile
                user.password = encrypt
                user.first_name = society_name
                user.save()
                userid = User.objects.get(id=user.id)

                socobj = Addsociety()
                socobj.id = get_society_id()
                socobj.society_name = society_name
                socobj.mobile = mobile
                socobj.emailid = emailid
                socobj.password = encrypt
                socobj.registration_date = registration_date
                socobj.aadhaar_number = aadhaar_number
                socobj.pan_number = pan_number
                socobj.account_number = account_number
                socobj.account_name = account_name
                socobj.account_type = account_type
                socobj.bank_name = bank_name
                socobj.branch_name = branch_name
                socobj.ifsc_code = ifsc_code
                socobj.house_number = house_number
                socobj.street_name = street_name
                socobj.land_mark = land_mark
                socobj.city = city
                socobj.state = state
                socobj.pincode = pincode
                socobj.save()
                return Response("Add Society successfully Saved")
        except Exception as e:
            print(e)
            return Response("Registration Failed")

def get_society_id():
    count = Addsociety.objects.all().count()
    if count != 0:
        customid = "S" + str(count + 1)
    else:
        customid = "S1"
    return customid


def get_federation_id():
    count = Federation.objects.all().count()
    if count != 0:
        customid = "F" + str(count + 1)
    else:
        customid = "F1"
    return customid
