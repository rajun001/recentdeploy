# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
import datetime

from django.core import serializers
from rest_framework import status
from rest_framework.decorators import api_view

from rest_framework.response import Response

from federation.models import Federation
from federation.models import Addsociety




from administration.models import AdminLogin, Token, StateInfo

from society.models import Society

from .functions import random_number, token, encrypt_password, authentication,get_otp,sendSMS
from rest_framework.generics import UpdateAPIView
# import logging
# from pip import logger

from django.views.generic import TemplateView
# import logging
# from pip import logger
from django.shortcuts import render

class HomePageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)


class LinksPageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'links.html', context=None)

def get_admin_id():
    count = AdminLogin.objects.all().count()
    if count != 0:
        customid = "A" + str(count + 1)
    else:
        customid = "A1"
    return customid


@api_view(['POST'])
def admin_login(request):
    print("request object data", request.data)
    mob = request.data['mobile']
    password = request.data['password']
    emailid = request.data['emailid']
    full_name = request.data['full_name']
    encrypt = encrypt_password(password)
    login = AdminLogin()
    user = User()
    login.mobile = mob
    login.emailid = emailid
    login.full_name = full_name
    login.password = encrypt
    login.id = get_admin_id()
    login.save()
    user.username = mob
    user.email = emailid
    user.first_name = full_name
    user.password = encrypt
    user.save()
    return Response ("successfully saved")


@api_view(['POST'])
def login(request):
    print ("dataaaaaaaaaaa",request.data)
    mobile = request.data['mobile']
    password = encrypt_password(request.data['password'])



    user = User.objects.filter(username =mobile,password=password)


    if user:
        print ("hiiiiiiiiiiiiiiiiiiii")
        profile = AdminLogin.objects.filter(mobile = mobile, password = password)
        print("admiiiiiiiiiisttttttaion",profile)
        if not profile:
            profile = Federation.objects.filter(mobile = mobile, password = password)
            print("profileeeeeeeeee",profile)
            if not profile:
                profile = Addsociety.objects.filter(mobile=mobile, password=password)
                print("profile",profile)
        token_number = random_number()
        token(token_number, profile[0])
        data = {'role': profile[0].role, 'token': token_number}

        print("token data",data)

        # time = datetime.datetime.now()
        # logger.info("######################################################")
        # logger.info("User " + str(profile[0].id) + " Logged in Successfully @ " + str(time))
        # logger.info("##############/########################################")
        return Response(data, status=status.HTTP_200_OK)
    else:
        data = {'message': 'mobile number or password wrong', 'token': 'error'}
        return Response(data)


@api_view(['POST'])
def logout(request):
    token_number = Token.objects.get(token=request.META['HTTP_AUTHORIZATION'])
    print(token_number.user)
    token_number.delete()
    time = datetime.datetime.now()
    # logger.info("######################################################")
    # logger.info("User " + token_number.user + " logged out successfully @ " + str(time))
    # logger.info("######################################################")
    return Response('loggedout', status=status.HTTP_200_OK)


# Registrations states
@api_view(['POST'])
def state(request):
    # id = get_state_id()
    def get_state_id():
        count = StateInfo.objects.all().count()
        print("sadfdsgfdg",count)
        if count != 0:
            st = "S" + str(count + 1)
        else:
            st = 1
        return str(st)
    state = request.data['state']
    obj= StateInfo()
    obj.stateid = get_state_id()
    obj.state = state
    obj.save()
    return Response("success")


@api_view(['GET'])
def get_state(request):
    a=StateInfo.objects.filter().values()
    print(a,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    return Response(a)




@api_view(['POST'])
def update_password(request):
    print (request.data)
    password = request.data['password']
    mobile_number = request.data['mobile_number1']
    encrypt = encrypt_password(password)
    user = User.objects.filter(username = mobile_number).update(password = encrypt)
    return Response("password updated successfully",status=status.HTTP_200_OK)

@api_view(['POST'])
def verify_otp(request):
    print (request.data)
    otp_fe = str(request.data['password'])
    otp_db = User.objects.filter(last_name=otp_fe)
    print("verified dataaaaaaaaaaaaa",otp_db)
    if otp_db:
        otp_db[0].last_name = ""
        otp_db[0].is_active = True
        otp_db[0].save()
        a=1
        msg = {"message":"OTP verified","mobilenumber":otp_db[0].username}
        data = {'a':a, 'msg':msg}
        return Response(data, status=status.HTTP_200_OK)
    else:
        return Response("Invalid OTP", status=status.HTTP_200_OK)

@api_view(['POST'])
def verification_otp(request):
    print (request.data)
    otp_fe = request.data['otp']
    otp_db = User.objects.filter(last_name=otp_fe)
    if otp_db:
        otp_db[0].last_name = ""
        otp_db[0].is_active = True
        otp_db[0].save()
        return Response("OTP Verified", status=status.HTTP_200_OK)
    else:
        return Response("Invalid OTP", status=status.HTTP_200_OK)

@api_view(['POST'])
def forgot_password(request):
    print("reqqqqqqqqqqqqqqqqqqqqq",request.data)
    mobile_number = request.data['mobile']

    try:
        user = User.objects.get(username=mobile_number)
        print("passsssssssssssssssswwwwwwwwwwwwwwwwwwooooooooooooooooorrrrrrr",user)
        otp = get_otp()
        sendSMS(otp, user.username)
        user.last_name = otp
        user.save()
        a=1
        msg="Password rest OTP sent"
        data = {'msg':msg, 'a':a}
        return Response(data, status=status.HTTP_200_OK)
    except Exception as e:
        print ("Exception raised")
        print (e)
        return Response("Enter the valid mobile number", status=status.HTTP_200_OK)


@api_view(['POST'])
def reset_password(request):
    mobile_number = request.data["mobile_number"]
    password = request.data["password"]
    encrypt = encrypt_password(password)

    user = User.objects.get(username=mobile_number)
    user.password = encrypt
    user.save()

    return Response("Password has been successfully reset", status=status.HTTP_200_OK)













