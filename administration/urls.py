from django.conf.urls import url, include
from django.contrib import admin
from .views import *
from administration import views
urlpatterns = [
url(r'^$', views.HomePageView.as_view()),
    url(r'^links/$', views.LinksPageView.as_view()),
    url(r'^saveadminlogin',admin_login),
    url(r'^loginusers',login),
    url(r'^forgotpassword',forgot_password),
url(r'^verify_otp',verify_otp),
    url(r'^resetpassword',reset_password),
    url(r'^addstate',state),
    url(r'^get_state', get_state),




    ]