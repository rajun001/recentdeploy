from django.db import models

# Create your models here.



class AdminLogin(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    full_name = models.CharField(max_length=50)
    mobile = models.BigIntegerField()
    emailid = models.EmailField(max_length=50, null=True)
    password = models.TextField(max_length=20)
    role = models.CharField(max_length=20,null=True, blank=True, default="ADMIN")


class Token(models.Model):
    token = models.CharField(max_length=50, null=True, blank=True)
    user = models.TextField(null=True, blank=True)
    role = models.CharField(max_length=50, null=True, blank=True)

class StateInfo(models.Model):
    stateid = models.CharField(max_length=50)
    state = models.CharField(max_length=50)